{
  description = "Hyper-V VM";

  inputs.nixpkgs.url = "github:Nixos/nixpkgs/nixos-21.05";

  outputs = { self, nixpkgs }: {

    nixosConfigurations."nixos-hyper-v" = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        ({ ... }: {
          nix.registry.nixpkgs.flake = nixpkgs;
          system.configurationRevision = nixpkgs.lib.mkIf (self ? rev) self.rev;
        })
        modules/hardware-configuration.nix
        modules/system.nix
        modules/kde.nix
      ];
    };

  };
}
