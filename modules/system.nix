{ pkgs, ... }:
{
  boot.blacklistedKernelModules = [ "hyperv_fb" ];
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos-hyper-v";
  networking.useDHCP = false;
  networking.interfaces.eth0.useDHCP = true;

  i18n.defaultLocale = "en_GB.UTf-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "uk";
  };

  users.users.simon = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDB/zhxjERr6Fa/8cmCdZfukMytG0ogNDGNo8npvTWHSMJLAIXtTn7LIo4DOqgs0v4nZCC+rZ2Z9Qa5zJwbGt9DV8gyFT10iauBGGfo1QMdfIecBqRkTdV0KrXed1SfMlhofD4fwaKtJrRc6+iu1anAUnoThlCGv5iHNsL6xvO3IY9GYhGKN/oS9XvboneCUAL2Z3AIyYEOTdbXIbq79mhFEJZAX3tf4sIyKG/aZ4+fNeV5wg6EuQAP24ofHfEYaf1pWYQhCl4+oOrkmqeW6CAoIIUN7/4rIYm7Hsd/Vqot8rNrCTFOc1RhR2ipCfWKzZnsZnwvi8C7gGy4mQY+Vn69 simon@DESKTOP-5HQP7EM" ];
  };

  services.openssh = {
    enable = true;
    passwordAuthentication = false;
    permitRootLogin = "no";
  };

  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  system.stateVersion = "21.05";
}
